import 'package:easy_localization/easy_localization.dart';
import 'package:gester_be/presentation/page/welcome/widget.dart';
import 'package:gester_be/presentation/res/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    EasyLocalization(
        supportedLocales: <Locale>[Locale('en'), Locale('uk')],
        path: 'assets/translations',
        fallbackLocale: Locale('uk'),
        startLocale: Locale('en'),
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    return MaterialApp(
      title: 'Gester - Admin Panel',
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      theme: ThemeData(
        accentColor: GesterColorPalette.greenDark,
        primarySwatch: Colors.green,
        textTheme: GoogleFonts.ralewayTextTheme(textTheme),
      ),
      debugShowCheckedModeBanner: false,
      home: WelcomePage(),
    );
  }
}
