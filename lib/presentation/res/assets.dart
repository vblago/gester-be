class GesterAssets{
  static const String logo = "assets/images/logo.png";
  static const String newUserGif = "assets/images/new-user.gif";
  static const String newEquipmentGif = "assets/images/new-equipment.gif";
  static const String assignGif = "assets/images/assign.gif";
  static const String logoIcon = "assets/icons/logo.svg";
}