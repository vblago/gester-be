import 'package:flutter/material.dart';

class GesterColorPalette {
  static const Color greenLight = Color(0xff388e3c);
  static const Color green = Color(0xff2e7d32);
  static const Color greenDark = Color(0xff1b5e20);
  static const Color lime = Color(0xffb9f6ca);
  static const Color purple = Color(0xff6a1b9a);
}
