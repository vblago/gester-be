class User {
  final String name;
  final String email;
  final List<String> availableRooms;

  User(this.name, this.email, this.availableRooms);
}
