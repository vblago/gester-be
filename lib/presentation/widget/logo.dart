import 'package:flutter/material.dart';
import 'package:gester_be/presentation/res/assets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:easy_localization/easy_localization.dart';

class LogoWidget extends StatelessWidget {
  final Color color;
  final MainAxisAlignment alignment;

  const LogoWidget({
    @required this.color,
    this.alignment = MainAxisAlignment.center,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: alignment,
      children: <Widget>[
        Image.asset(
          GesterAssets.logo,
          width: 50,
          height: 50,
        ),
        Text(
          "app_name",
          style: GoogleFonts.raleway(
            textStyle: Theme.of(context).textTheme.headline4,
            fontSize: 32,
            fontWeight: FontWeight.w700,
            color: color,
          ),
        ).tr(),
      ],
    );
  }
}
