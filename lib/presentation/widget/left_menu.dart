import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gester_be/presentation/res/colors.dart';
import 'package:easy_localization/easy_localization.dart';

class LeftMenu extends StatelessWidget {
  final List<MenuItem> items;

  const LeftMenu({
    this.items,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 25),
      width: 200,
      height: double.maxFinite,
      decoration: BoxDecoration(
        color: GesterColorPalette.greenLight,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 4,
            offset: Offset(1, 1),
          ),
        ],
      ),
      child: Column(
        children: items
            .map<Widget>((MenuItem item) => _menuItem(item, context))
            .toList(),
      ),
    );
  }

  Widget _menuItem(MenuItem item, BuildContext context) {
    return CupertinoButton(
      padding: const EdgeInsets.only(bottom: 20),
      minSize: 0,
      onPressed: item.onTap,
      child: Row(
        children: <Widget>[
          Icon(item.icon, color: Colors.white),
          SizedBox(width: 10),
          Text(
            item.title,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ).tr(),
        ],
      ),
    );
  }
}

class MenuItem {
  final String title;
  final IconData icon;
  final GestureTapCallback onTap;

  MenuItem(this.title, this.icon, this.onTap);
}
