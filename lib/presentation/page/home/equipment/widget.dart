import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gester_be/presentation/res/assets.dart';
import 'package:gester_be/presentation/res/colors.dart';
import 'package:gester_be/presentation/widget/dialog/asset.dart';
import 'package:gester_be/presentation/widget/dialog/base.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:easy_localization/easy_localization.dart';

class EquipmentWidget extends StatelessWidget {
  final TextEditingController _brandTC = TextEditingController();
  final TextEditingController _modelTC = TextEditingController();
  final TextEditingController _goalTC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitle(context),
        SizedBox(height: 20),
        _createNewItemButton(context),
        SizedBox(height: 20),
        _buildColumnTitles(),
        SizedBox(height: 5),
        Container(
          width: double.maxFinite,
          height: 1,
          color: GesterColorPalette.greenDark,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('equipment').snapshots(),
            builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  padding: const EdgeInsets.only(top: 20),
                  itemBuilder: (_, int index) {
                    return _buildItem(
                      snapshot.data.documents[index],
                      index % 2 == 0 ? GesterColorPalette.lime : null,
                      context,
                    );
                  },
                  itemCount: snapshot.data.documents.length,
                );
              } else {
                return Center(
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      "equipment",
      style: GoogleFonts.raleway(
        textStyle: Theme.of(context).textTheme.headline4,
        fontSize: 24,
        fontWeight: FontWeight.w700,
        color: GesterColorPalette.green,
      ),
    ).tr();
  }

  Widget _buildColumnTitles() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 150,
            child: Text(
              "brand",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          SizedBox(
            width: 200,
            child: Text(
              "model",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          SizedBox(
            width: 200,
            child: Text(
              "assign_to",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          SizedBox(
            width: 150,
            child: Text(
              "main_goal",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          Spacer(),
          SizedBox(
            width: 350,
            child: Text(
              "actions",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(
    DocumentSnapshot equipmentSnapshot,
    Color color,
    BuildContext context,
  ) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      height: 70,
      color: color,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 150,
            child: Text(
              equipmentSnapshot["brand"] as String,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            width: 200,
            child: Text(
              equipmentSnapshot["model"] as String,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            width: 250,
            child: Text(
              (equipmentSnapshot["assignToEmail"] as String) ?? "",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            width: 130,
            child: Text(
              equipmentSnapshot["goal"] as String,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Spacer(),
          SizedBox(
            width: 350,
            child: Row(
              children: <Widget>[
                _buildButton(
                  "edit",
                  Colors.black,
                  FontAwesomeIcons.edit,
                  () => _editEquipment(context, equipmentSnapshot),
                ),
                SizedBox(width: 15),
                _buildButton(
                  "assign",
                  GesterColorPalette.greenDark,
                  FontAwesomeIcons.userCheck,
                  () => _assignEquipment(context, equipmentSnapshot),
                ),
                SizedBox(width: 15),
                _buildButton(
                  "delete",
                  Colors.redAccent,
                  FontAwesomeIcons.trash,
                  () => Firestore.instance
                      .collection("equipment")
                      .document(equipmentSnapshot.documentID)
                      .delete(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  CupertinoButton _buildButton(
    String title,
    Color color,
    IconData icon,
    GestureTapCallback onTap,
  ) {
    return CupertinoButton(
      minSize: 0,
      padding: EdgeInsets.zero,
      onPressed: onTap,
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: color,
          ),
          SizedBox(width: 5),
          Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: color,
            ),
          ).tr(),
        ],
      ),
    );
  }

  Widget _createNewItemButton(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      minSize: 0,
      onPressed: () => _createEquipment(context),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              GesterColorPalette.greenDark,
              GesterColorPalette.green
            ],
          ),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(FontAwesomeIcons.plusCircle, color: Colors.white),
            SizedBox(width: 10),
            Text(
              'create_equipment',
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ).tr(),
          ],
        ),
      ),
    );
  }

  void _createEquipment(BuildContext context) {
    _changeEquipment(context, 'Equipment edit', () async {
      await Firestore.instance.collection("equipment").add(<String, String>{
        "brand": _brandTC.text,
        "model": _modelTC.text,
        "goal": _goalTC.text
      });
      Navigator.pop(context);
    });
  }

  void _editEquipment(BuildContext context, DocumentSnapshot document) {
    _brandTC.text = document["brand"] as String;
    _modelTC.text = document["model"] as String;
    _goalTC.text = document["goal"] as String;

    _changeEquipment(context, 'Equipment edit', () async {
      await Firestore.instance
          .collection("equipment")
          .document(document.documentID)
          .updateData(<String, String>{
        "brand": _brandTC.text,
        "model": _modelTC.text,
        "goal": _goalTC.text
      });
      Navigator.pop(context);
    });
  }

  void _changeEquipment(
    BuildContext context,
    String title,
    GestureTapCallback onTap,
  ) {
    showDialog<void>(
      context: context,
      builder: (_) => AssetGiffyDialog(
        image: Image.asset(GesterAssets.newEquipmentGif,
            height: 150, fit: BoxFit.fitWidth),
        title: Text(
          title,
          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
        ),
        description: Column(
          children: <Widget>[
            _entryField("brand", _brandTC),
            _entryField("model", _modelTC),
            _entryField("main_goal", _goalTC),
          ],
        ),
        buttonOkText: Text(
          "Save",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        onOkButtonPressed: onTap,
        entryAnimation: EntryAnimation.TOP,
      ),
    );
  }

  Widget _entryField(String title, TextEditingController controller) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ).tr(),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: controller,
            cursorColor: GesterColorPalette.greenDark,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }

  void _assignEquipment(
    BuildContext context,
    DocumentSnapshot equipment,
  ) {
    showDialog<void>(
      context: context,
      builder: (_) => AssetGiffyDialog(
        image: Image.asset(GesterAssets.assignGif,
            height: 150, fit: BoxFit.fitWidth),
        title: Text(
          "Assign Equipment",
          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
        ),
        description: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance.collection('users').snapshots(),
          builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              final List<DocumentSnapshot> documents = snapshot.data.documents
                  .where((DocumentSnapshot snapshot) =>
                      (snapshot["role"] as String) == "user")
                  .toList();
              return SizedBox(
                height: 250,
                child: ListView.builder(
                  padding: const EdgeInsets.only(top: 20),
                  itemBuilder: (_, int index) {
                    return CupertinoButton(
                      minSize: 0,
                      padding: EdgeInsets.zero,
                      onPressed: () async {
                        await Firestore.instance
                            .collection("equipment")
                            .document(equipment.documentID)
                            .updateData(<String, String>{
                          "assignToEmail": documents[index]["email"] as String,
                          "assignToId": documents[index].documentID,
                        });
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        height: 70,
                        color: index % 2 == 0 ? GesterColorPalette.lime : null,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.user,
                              color: Colors.black,
                            ),
                            SizedBox(width: 10),
                            Text(
                              documents[index]["name"] as String,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: documents.length,
                ),
              );
            } else {
              return Center(
                child: SizedBox(
                  width: 30,
                  height: 30,
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
        onlyCancelButton: true,
        entryAnimation: EntryAnimation.TOP,
      ),
    );
  }
}
