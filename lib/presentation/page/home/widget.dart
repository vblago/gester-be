import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gester_be/presentation/model/user.dart';
import 'package:gester_be/presentation/page/home/equipment/widget.dart';
import 'package:gester_be/presentation/page/home/users/widget.dart';
import 'package:gester_be/presentation/page/welcome/widget.dart';
import 'package:gester_be/presentation/res/colors.dart';
import 'package:gester_be/presentation/widget/left_menu.dart';
import 'package:gester_be/presentation/widget/logo.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeWidget extends StatefulWidget {
  final User user;

  const HomeWidget({
    @required this.user,
    Key key,
  }) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  Widget currentWidget = UsersWidget();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: GesterColorPalette.greenDark,
        title: _buildAppBar(context),
      ),
      body: Row(
        children: <Widget>[
          LeftMenu(
            items: <MenuItem>[
              MenuItem("users", FontAwesomeIcons.user,
                  () => setState(() => currentWidget = UsersWidget())),
              MenuItem(
                  "admins",
                  FontAwesomeIcons.userEdit,
                  () => setState(
                      () => currentWidget = UsersWidget(isAdmin: true))),
              MenuItem("equipment", FontAwesomeIcons.film,
                  () => setState(() => currentWidget = EquipmentWidget())),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 25, right: 25),
              child: currentWidget,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: LogoWidget(
            color: Colors.white,
            alignment: MainAxisAlignment.start,
          ),
        ),
        CupertinoButton(
          padding: EdgeInsets.zero,
          minSize: 0,
          onPressed: () async {
            if (context.locale.toString() == "en") {
              context.locale = Locale("uk");
            } else {
              context.locale = Locale("en");
            }
            setState(() {});
          },
          child: Text(
            "change_localization",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ).tr(),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "•",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Text(
          widget.user.name,
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "•",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        CupertinoButton(
          padding: EdgeInsets.zero,
          minSize: 0,
          onPressed: () async {
            await FirebaseAuth.instance.signOut();
            Navigator.of(context).pop();
            Navigator.push<void>(
                context,
                MaterialPageRoute<void>(
                    builder: (BuildContext context) => WelcomePage()));
          },
          child: Text(
            "log_out",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ).tr(),
        ),
      ],
    );
  }
}
