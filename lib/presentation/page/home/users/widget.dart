import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gester_be/presentation/res/assets.dart';
import 'package:gester_be/presentation/res/colors.dart';
import 'package:gester_be/presentation/widget/dialog/asset.dart';
import 'package:gester_be/presentation/widget/dialog/base.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:easy_localization/easy_localization.dart';

class UsersWidget extends StatelessWidget {
  final TextEditingController _nameTC = TextEditingController();

  final bool isAdmin;

  UsersWidget({Key key, this.isAdmin = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitle(context),
        SizedBox(height: 20),
        _buildColumnTitles(),
        SizedBox(height: 5),
        Container(
          width: double.maxFinite,
          height: 1,
          color: GesterColorPalette.greenDark,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('users').snapshots(),
            builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                final List<DocumentSnapshot> documents = snapshot.data.documents
                    .where((DocumentSnapshot snapshot) =>
                        (snapshot["role"] as String) ==
                        (isAdmin ? "admin" : "user"))
                    .toList();
                return ListView.builder(
                  padding: const EdgeInsets.only(top: 20),
                  itemBuilder: (_, int index) {
                    return _buildItem(
                      documents[index],
                      index % 2 == 0 ? GesterColorPalette.lime : null,
                      context,
                    );
                  },
                  itemCount: documents.length,
                );
              } else {
                return Center(
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      isAdmin ? "admin_edit" : "user_edit",
      style: GoogleFonts.raleway(
        textStyle: Theme.of(context).textTheme.headline4,
        fontSize: 24,
        fontWeight: FontWeight.w700,
        color: GesterColorPalette.green,
      ),
    ).tr();
  }

  Widget _buildColumnTitles() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 200,
            child: Text(
              "full_name",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          Expanded(
            child: Text(
              "email",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
          SizedBox(
            width: 220,
            child: Text(
              "actions",
              style: TextStyle(
                fontSize: 16,
              ),
            ).tr(),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(
    DocumentSnapshot userSnapshot,
    Color color,
    BuildContext context,
  ) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      height: 70,
      color: color,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 200,
            child: Text(
              userSnapshot["name"] as String,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Expanded(
            child: Text(
              userSnapshot["email"] as String,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            width: 220,
            child: Row(
              children: <Widget>[
                _buildButton(
                  "edit",
                  Colors.black,
                  FontAwesomeIcons.edit,
                  () => _editUser(context, userSnapshot),
                ),
                SizedBox(width: 15),
                _buildButton(
                  "delete",
                  Colors.redAccent,
                  FontAwesomeIcons.trash,
                  () => Firestore.instance
                      .collection("users")
                      .document(userSnapshot.documentID)
                      .delete(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  CupertinoButton _buildButton(
    String title,
    Color color,
    IconData icon,
    GestureTapCallback onTap,
  ) {
    return CupertinoButton(
      minSize: 0,
      padding: EdgeInsets.zero,
      onPressed: onTap,
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: color,
          ),
          SizedBox(width: 5),
          Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: color,
            ),
          ).tr(),
        ],
      ),
    );
  }

  void _editUser(BuildContext context, DocumentSnapshot document) {
    _nameTC.text = document["name"] as String;
    showDialog<void>(
      context: context,
      builder: (_) => AssetGiffyDialog(
        image: Image.asset(GesterAssets.newUserGif),
        title: Text(
          'user_edit',
          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
        ).tr(),
        description: Column(
          children: <Widget>[
            _entryField("full_name", _nameTC),
          ],
        ),
        buttonOkText: Text(
          "save",
          style: TextStyle(
            color: Colors.white,
          ),
        ).tr(),
        onOkButtonPressed: () async {
          await Firestore.instance
              .collection("users")
              .document(document.documentID)
              .updateData(<String, String>{"name": _nameTC.text});
          Navigator.pop(context);
        },
        entryAnimation: EntryAnimation.TOP,
      ),
    );
  }

  Widget _entryField(String title, TextEditingController controller) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ).tr(),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: controller,
            cursorColor: GesterColorPalette.greenDark,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }
}
